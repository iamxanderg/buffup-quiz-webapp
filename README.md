# Technical Test

## About this project

Technical Test

## Contents

- [Getting Started](#getting-started)

## Getting Started

These instructions will get a copy of the project up and running on your local machine for development and testing purposes.

```bash
# Clone the project to your machine
git clone https://gitlab.com/iamxander/shopworks-tech-test.git

# Navigate to the project folder
cd shopworks-tech-test

# Run the install
npm install

# Build the base project
npm run build

# Start the development server
npm start
```
