const path = require("path");

module.exports = {
    fromRoot: relativePath => path.resolve(__dirname, relativePath)
}