const { presets, plugins } = require("../_webpack/webpack.config.babel");

module.exports = require("babel-jest").createTransformer({ presets, plugins });
