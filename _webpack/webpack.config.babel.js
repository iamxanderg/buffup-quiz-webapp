exports.presets = [
  ["@babel/preset-env", { targets: { node: "current", ie: 11 } }],
  "@babel/preset-react",
  "@babel/preset-typescript",
];

exports.plugins = [
  "@babel/plugin-proposal-object-rest-spread",
  "@babel/plugin-proposal-class-properties",
];
