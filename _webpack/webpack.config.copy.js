const paths = require("../paths");

module.exports = [
  {
    context: paths.fromRoot("src/assets/data"),
    from: "**/*",
    to: "data",
  },
];
