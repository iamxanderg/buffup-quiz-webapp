module.exports = {
  roots: ["<rootDir>/src"],
  errorOnDeprecated: true,
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|svg|eot|otf|webp|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|m4v|aac|oga)$":
      "<rootDir>/_test/jest.fileMock.js",
    "\\.(css|scss|sass)$": "<rootDir>/_test/jest.styleMock.js",
  },
  setupFilesAfterEnv: ["<rootDir>/_test/jest.setupTests.js"],
  snapshotSerializers: ["enzyme-to-json/serializer"],
  testMatch: ["**/src/**/*.(spec|test).(js|ts)?(x)"],
  transform: {
    "^.+\\.jsx?$": "<rootDir>/_test/jest.transform.js",
    "^.+\\.tsx?$": "ts-jest",
  },
};
