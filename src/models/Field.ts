import { ChangeEvent } from "react";

export interface Field {
    name: string;
    label: string;
    type?: string;
    options?: string[];
    onChange?: (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void;
  }
  