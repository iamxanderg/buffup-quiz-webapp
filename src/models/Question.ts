import { Field } from "./Field";

export interface Question {
    title: string;
    fields: Field[];
  }
  