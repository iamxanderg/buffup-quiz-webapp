import * as React from "react";
import * as ReactDOM from "react-dom";
import QuestionForm from "./components/question-form";

ReactDOM.render(
  <QuestionForm />,
  document.getElementById("root")
);
