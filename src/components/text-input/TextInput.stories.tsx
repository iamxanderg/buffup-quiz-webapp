import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import TextInput from "./index";
import { Field } from "../../models/Field";

export default {
  title: "Components/TextInput",
  component: TextInput,
  argTypes: {
    name: { control: "text" },
    label: { control: "text" },
    type: { control: "text" },
    onChange: { action: "onChange" },
  },
} as Meta;

const Template: Story<Field> = (args) => <TextInput {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  name: "first_name",
  label: "First Name",
  type: "text"
};
