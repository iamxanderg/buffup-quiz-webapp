import * as React from "react";
import { shallow } from "enzyme";
import TextInput from "./index";


describe("TextInput component", () => {
  let wrapper: any;

  beforeEach(() => {
    wrapper = shallow(<TextInput name="first_name" label="First Name" type="text" />);
  })

  it("should have value 'Obi-Wan'", () => {
    const textInput = wrapper.find("#first_name");
    textInput.value = "Obi-Wan"
    expect(textInput.value).toBe("Obi-Wan");
  });

  it("should not have value 'Obi-Wan'", () => {
    const textInput = wrapper.find("#first_name");
    textInput.value = "Han Solo"
    expect(textInput.value).not.toBe("Obi-Wan")
 });

 it("should have selected value 'Luke Skywalker'", () => {
    const onChangeMock = jest.fn();
    const event = "Luke Skywalker";

    const wrapper = shallow(<TextInput name="first_name" label="First Name" type="text" onChange={onChangeMock} />);
    wrapper.find("#first_name").simulate("change", event);
    expect(onChangeMock).toBeCalledWith("Luke Skywalker");
  });
});
