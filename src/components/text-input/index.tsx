import * as React from "react";
import { Field } from "../../models/Field";
import "./text-input.scss";

const TextInput = (props: Field) => {
    const { name, label, type, onChange } = props

    return (
        <div className="textField">
            <label htmlFor={name} className="textField__label">{label}</label>
            <input id={name} name={name} type={type} aria-labelledby={label} onChange={onChange} className="textField__input" />
        </div>
    )
}

export default TextInput;