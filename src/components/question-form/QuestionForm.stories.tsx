import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import QuestionForm from "./index";

export default {
  title: "Form/Question Form",
  component: QuestionForm,
} as Meta;

const Template: Story = () => <QuestionForm />;

export const Rendered = Template.bind({});
