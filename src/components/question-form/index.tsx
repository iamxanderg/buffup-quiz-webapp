import * as React from "react";
import { Component, ChangeEvent } from "react";
import TextInput from "../text-input";
import Dropdown from "../dropdown";
import { Field } from "../../models/Field";
import * as mockData from "../../assets/data/questions.json";
import "./question-form.scss";

interface IProps {}

interface IState {
    formValues: {}
}

export default class QuestionForm extends Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);

        this.state = {
            formValues: {}
        };
    }

    private updateDetails = (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>): void =>  {
        const key = e.currentTarget.name;
        const val = e.currentTarget.value;

        this.setState( (prevState: any) => {
            const { formValues } = prevState;
            formValues[key] = val;
        });
    }

    private printState = () => {
        const { formValues } = this.state;
        
        for (let [key, value] of Object.entries(formValues)) {
            console.log(`${key}: ${value}`);
        }
    }

    private renderFields = (fields: Field[]) => {
        return fields.map((field) => {
            const { name, label, type, options } = field;

            if (type === "dropdown") {
                return <Dropdown key={name} name={name} label={label} options={options} onChange={this.updateDetails} />
            } else {
                return <TextInput key={name} name={name} label={label} type={type} onChange={this.updateDetails} />
            }
        })
    }

    private renderQuestion = () => {
        return mockData.questions.map((question, index) => {
            const { title, fields } = question;
            return (
                <section key={index} className="question question__wrapper">
                    <h3>{title}</h3>
                    {this.renderFields(fields)}
                </section>
            )
        })
    }

    render() {
        return (
            <div className="container">
                <h1>Question Form</h1>
                {this.renderQuestion()}
                <button type="button" onClick={() => this.printState()} className="question__button">Print Current State</button>
            </div>
        )
    }
    
}