import React from "react";
import { Story, Meta } from "@storybook/react/types-6-0";
import Dropdown from "./index";
import { Field } from "../../models/Field";

export default {
  title: "Components/Dropdown",
  component: Dropdown,
  argTypes: {
    name: { control: "text" },
    label: { control: "text" },
    options: { control: "object" },
    onChange: { action: "onChange" },
  },
} as Meta;

const Template: Story<Field> = (args) => <Dropdown {...args} />;

export const Rendered = Template.bind({});
Rendered.args = {
  name: "country",
  label: "Country",
  options: ["Canada", "USA"],
};
