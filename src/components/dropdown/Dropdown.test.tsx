import * as React from "react";
import { shallow } from "enzyme";
import Dropdown from "./index";

describe("Dropdown component", () => {

  it("should have selected value 'Canada'", () => {
    const onChangeMock = jest.fn();
    const event = "Canada";

    const wrapper = shallow(<Dropdown name="country" label="Country" options={["Canada", "USA"]} onChange={onChangeMock} />);
    wrapper.find("#country").simulate("change", event);
    expect(onChangeMock).toBeCalledWith("Canada");
  });

  it("should not have selected value 'Canada'", () => {
    const onChangeMock = jest.fn();
    const event = "USA";

    const wrapper = shallow(<Dropdown name="country" label="Country" options={["Canada", "USA"]} onChange={onChangeMock} />);
    wrapper.find("#country").simulate("change", event);
    expect(onChangeMock).not.toBeCalledWith("Canada");
  });
});
