import * as React from "react";
import { Field } from "../../models/Field";
import "./dropdown.scss";

const Dropdown = (props: Field) => {
    const { name, label, options, onChange } = props
    const defaultOption = options.length > 0 ? options[0] : "";

    return (
        <div className="ddField">
            <label htmlFor={name} className="ddField__label">{label}</label>
            <select id={name} name={name} aria-labelledby={label} value={defaultOption} onChange={onChange} className="ddField__select">
                {
                options.map(option => {
                    return <option key={option} value={option}>{option}</option>
                })
                }
            </select>
        </div>
    )
}

export default Dropdown;